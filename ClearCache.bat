
rd /s /q ".vs"
rd /s /q "Binaries"
rd /s /q "Build"
rd /s /q "DerivedDataCache"
rd /s /q "Intermediate"

@echo F|xcopy /S /Q /Y /F "Saved/Config/Windows/EditorPerProjectUserSettings.ini" "EditorPerProjectUserSettings.ini"
rd /s /q "Saved"
@mkdir "Saved/Config/Windows"
move "EditorPerProjectUserSettings.ini" "Saved/Config/Windows/"
del /q "*.sln"

@if exist "Plugins" (
@cd "Plugins"
@for /f "delims=" %%D in ('dir /b /s "Intermediate"') do (
RD /s /q %%D
)
)

@cd ../

pause