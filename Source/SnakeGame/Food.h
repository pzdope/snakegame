// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeBase.h"
#include "Food.generated.h"

UCLASS()
class SNAKEGAME_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void GenerateField();
	
	void EatFood(ASnakeBase* SnakeBase);

	TArray<FVector> AllLocations;

	float FieldX;
	float FieldY;
	float FieldCenter;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	
	/*UFUNCTION()
	static FVector2D GetRandomLocation();*/

	UFUNCTION()
	FVector TraceCheckedLocation();

	UFUNCTION()
	void RespawnFood();

	/*UPROPERTY(EditAnywhere, Category="Location")
	FVector FieldLocation;

	UPROPERTY(EditAnywhere, Category="Location")
	FQuat FieldRotation;*/
};
