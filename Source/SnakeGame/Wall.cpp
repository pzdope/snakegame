// Fill out your copyright notice in the Description page of Project Settings.


#include "Wall.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
AWall::AWall()
{
	PrimaryActorTick.bCanEverTick = true;
	
}

void AWall::BeginPlay()
{
	Super::BeginPlay();
	
}

void AWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void AWall::Interact(AActor* Interactor, bool bIsHead)
{
	IInteractable::Interact(Interactor, bIsHead);

	const auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		/*if (const APlayerPawnBase* PlayerPawnBase = Interactor->GetInstigator()->GetController()->GetPawn<APlayerPawnBase>())
		{
			//PlayerPawnBase->PlayPortalSound(GetActorLocation());
		}*/
		
		if (!Snake->InPortal)
		{
			Snake->InPortal = true;

			FVector MovementVector(FVector::ZeroVector);
			
			switch (Snake->LastMoveDirection)
			{
			case EMovementDirection::UP:
				MovementVector.X -= PortalDistance;
				break;
			case EMovementDirection::DOWN:
				MovementVector.X += PortalDistance;
				break;
			case EMovementDirection::LEFT:
				MovementVector.Y -= PortalDistance;
				break;
			case EMovementDirection::RIGHT:
				MovementVector.Y += PortalDistance;
				break;
			}
			Snake->SnakeElements[0]->AddActorWorldOffset(MovementVector);
			Snake->InPortal = false;
		}
	}
}

