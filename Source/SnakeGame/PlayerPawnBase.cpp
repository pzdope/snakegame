// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"


APlayerPawnBase::APlayerPawnBase()
{
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	
	const FName CurrentLevelName = FName(UGameplayStatics::GetCurrentLevelName(GetWorld()));
	if (CurrentLevelName != FName("MainMenu"))
	{
		SetActorRotation(FRotator(-70,180,0));
		CreateSnakeActor();
	}

	FInputModeGameOnly InputModeData;
	InputModeData.SetConsumeCaptureMouseDown(true);
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	PlayerController->SetInputMode(InputModeData);
}

void APlayerPawnBase::CreateGameOverWidget()
{
	GameOverWidget = CreateWidget<UGameOverWidget>(GetWorld(),GameOverWidgetClass);
	if(GameOverWidget)
	GameOverWidget->Setup();
}

void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Instigator = this;
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform(), SpawnParameters);
}

/*void APlayerPawnBase::PlayEatSound(FVector Location)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), EatSound, Location);
}

void APlayerPawnBase::PlayDiedSound(FVector Location)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), DiedSound, Location);
}*/

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if(IsValid(SnakeActor) && (SnakeActor->bCanTurn == false))
	{
		if(value>0 && SnakeActor->LastMoveDirection!=EMovementDirection::UP)
		{
			SnakeActor->bCanTurn = true;
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
		else if (value<0 && SnakeActor->LastMoveDirection!=EMovementDirection::DOWN)
		{
			SnakeActor->bCanTurn = true;
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
	}
	
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if(IsValid(SnakeActor) && SnakeActor->bCanTurn == false)
	{
		if(value>0 && SnakeActor->LastMoveDirection!=EMovementDirection::RIGHT)
		{
			SnakeActor->bCanTurn = true;
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}
		else if (value<0 && SnakeActor->LastMoveDirection!=EMovementDirection::LEFT)
		{
			SnakeActor->bCanTurn = true;
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
	}
}
