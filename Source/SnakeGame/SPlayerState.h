// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "SPlayerState.generated.h"

class USSaveGame;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnScoreChanged, ASPlayerState*, PlayerState, int32, NewScore, int32, Delta);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnLivesChanged, ASPlayerState*, PlayerState, int32, NewLives, int32, Delta);
/**
 * 
 */
UCLASS()
class SNAKEGAME_API ASPlayerState : public APlayerState
{
	GENERATED_BODY()
	
protected:
	
	UPROPERTY(EditDefaultsOnly, Category= "Score")
	int32 PlayerScore = 0;

	int32 HighScore;

	int32 Lives = 3;

public:
	UFUNCTION(BlueprintCallable, Category= "Lives")
	int32 GetPlayerLives() const;
	
	UFUNCTION(BlueprintCallable, Category= "Lives")
	void TakeLive(int32 Delta);

	UFUNCTION(BlueprintCallable, Category= "Score")
	int32 GetPlayerScore() const;

	UFUNCTION(BlueprintCallable, Category= "Score")
	int32 GetHighScore() const;

	UFUNCTION(BlueprintCallable, Category= "Score")
	void AddScore (int32 Delta);

	UPROPERTY(BlueprintAssignable, Category= "Events")
	FOnScoreChanged OnScoreChanged;

	UPROPERTY(BlueprintAssignable, Category= "Events")
	FOnLivesChanged OnLivesChanged;

	UFUNCTION(BlueprintNativeEvent)
	void SavePlayerState(USSaveGame* SaveObject);

	UFUNCTION(BlueprintNativeEvent)
	void LoadPlayerState(USSaveGame* SaveObject);
};
