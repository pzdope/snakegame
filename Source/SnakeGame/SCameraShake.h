// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MatineeCameraShake.h"
#include "SCameraShake.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API USCameraShake : public UMatineeCameraShake
{
	GENERATED_BODY()

public:
	USCameraShake();
	
};
