// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "Camera/CameraShakeBase.h"
#include "GameOverWidget.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	APlayerPawnBase();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateGameOverWidget();

	void CreateSnakeActor();

	//void PlayEatSound(FVector Location);
	//void PlayDiedSound(FVector Location);

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);
	
	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

protected:
	UPROPERTY(BlueprintReadWrite)
		UGameOverWidget* GameOverWidget;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<UGameOverWidget> GameOverWidgetClass;

	/*UPROPERTY(EditDefaultsOnly, Category= "Sound")
		USoundCue* EatSound;

	UPROPERTY(EditDefaultsOnly, Category= "Sound")
		USoundCue* DiedSound;*/

	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase>SnakeActorClass;
};
