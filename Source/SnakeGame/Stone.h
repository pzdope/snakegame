// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeBase.h"
#include "Stone.generated.h"

class USCameraShake;

UCLASS()
class SNAKEGAME_API AStone : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStone();

	UPROPERTY(EditAnywhere)
		TSubclassOf<UCameraShakeBase> SCameraShake;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	void GenerateField();

	void TakeLive(ASnakeBase* SnakeBase);

	TArray<FVector> AllLocations;

	float FieldX;
	float FieldY;
	float FieldCenter;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	
	UPROPERTY(BlueprintReadWrite)
		AStone* StoneActor;

	UFUNCTION()
		FVector TraceCheckedLocation();

	UFUNCTION()
		void Respawn();

	UFUNCTION()
		void CameraShakeDemo(float Scale) const;
	
};
