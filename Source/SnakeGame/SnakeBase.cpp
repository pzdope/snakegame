// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "PlayerPawnBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	ElementSize = 100.0f;
	MovementSpeed = 0.5f;
	LastMoveDirection = EMovementDirection::DOWN;
}

void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	
	PlayerPawnBase = Cast<APlayerPawnBase>(GetInstigatorController()->GetPawn());
	
	SetActorTickInterval(MovementSpeed);
	
	AddSnakeElement(3);
}

void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	bCanTurn = true;
	Move();
}
											
void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	FVector PrevLocation = FVector::ZeroVector;
	
	for (int i = 0; i < ElementsNum; ++i)
	{
		for (int j = SnakeElements.Num() - 1; j > 0; j--)
		{
			PrevElement = SnakeElements[j];
			PrevLocation = PrevElement->GetActorLocation();	
		}
		
		FVector NewLocation(PrevLocation);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		const int32 ElementIndex = SnakeElements.Add(NewSnakeElement);
		if(ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
	}
}

ASnakeGameModeBase* ASnakeBase::GetGameMode() const
{
	ASnakeGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASnakeGameModeBase>();
	return GameMode;
}

void ASnakeBase::Destroyed()
{
	Super::Destroyed();
	if(PlayerPawnBase)
	{
		PlayerPawnBase->CreateGameOverWidget();
		//PlayerPawnBase->PlayDiedSound(GetActorLocation());
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(FVector::ZeroVector);
	
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	bCanTurn = false;
	
	SnakeElements[0]->ToggleCollision();
	for(int i = SnakeElements.Num() - 1; i > 0; --i)
	{
		const auto CurrentElement = SnakeElements[i];
		PrevElement = SnakeElements[i-1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		const bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::SetMovementSpeed(float SpeedValue){ MovementSpeed = SpeedValue; }
