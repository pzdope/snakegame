// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SnakeGameModeBase.h"
#include "GameOverWidget.generated.h"

class UButton;

UCLASS()
class SNAKEGAME_API UGameOverWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	
	void Setup();

	UPROPERTY(meta = (BindWidget))
		UButton* RestartButton;

	UPROPERTY(meta = (BindWidget))
		UButton* ExitButton;

	UFUNCTION()
		void RestartButtonClicked();

	UFUNCTION()
		void ExitButtonClicked();

	virtual void NativeConstruct() override;

	ASnakeGameModeBase* GetGameMode();

protected:

	UPROPERTY()
		APlayerController* PlayerController = nullptr;
};
